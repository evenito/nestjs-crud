import { join } from 'path';
import { DataSourceOptions } from 'typeorm';
import { isNil } from '@evenito/nestjs-crud-util';

const postgresHost = process.env.POSTGRES_HOST || '127.0.0.1';
const postgresPort = Number(process.env.POSTGRES_PORT) || 5455;
const redisHost = process.env.REDIS_HOST || '127.0.0.1';
const redisPort = process.env.REDIS_PORT || 6399;

export const ormConfig: DataSourceOptions = {
  type: 'postgres',
  host: postgresHost,
  port: postgresPort,
  username: 'root',
  password: 'root',
  database: 'nestjsx_crud',
  synchronize: false,
  logging: !isNil(process.env.TYPEORM_LOGGING)
    ? !!parseInt(process.env.TYPEORM_LOGGING, 10)
    : true,
  // cache: {
  //   type: 'redis',
  //   options: {
  //     host: '127.0.0.1',
  //     port: 6399
  //   },
  // },
  entities: [join(__dirname, './**/*.entity{.ts,.js}')],
  migrationsTableName: 'orm_migrations',
  migrations: ['./seeds.ts'],
};

export default ormConfig;
