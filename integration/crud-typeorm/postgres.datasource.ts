import {DataSource} from 'typeorm'
import {ormConfig} from './orm.config'

const PostgresDataSource = new DataSource(ormConfig);
export default PostgresDataSource;