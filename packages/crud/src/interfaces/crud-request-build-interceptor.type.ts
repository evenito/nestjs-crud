import { ExecutionContext } from '@nestjs/common';
import { CrudRequest } from './crud-request.interface';

export type CrudRequestBuildInterceptor = (
  context: ExecutionContext,
  req: CrudRequest,
) => CrudRequest;
