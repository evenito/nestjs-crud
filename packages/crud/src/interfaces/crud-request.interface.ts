import { ParsedRequestParams } from '@evenito/nestjs-crud-request';

import { CrudRequestOptions } from '../interfaces';

export interface CrudRequest {
  parsed: ParsedRequestParams;
  options: CrudRequestOptions;
  extra?: { [key: string]: any };
}
