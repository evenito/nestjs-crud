import { RequestQueryBuilder } from '@evenito/nestjs-crud-request';
import { isObjectFull } from '@evenito/nestjs-crud-util';
import * as deepmerge from 'deepmerge';

import { BaseRouteOptions, CrudGlobalConfig } from '../interfaces';

export class CrudConfigService {
  static config: CrudGlobalConfig = {
    auth: {},
    query: {
      alwaysPaginate: false,
    },
    operators: {},
    routes: {
      getManyBase: { interceptors: [], decorators: [] },
      getOneBase: { interceptors: [], decorators: [] },
      createOneBase: { interceptors: [], decorators: [], returnShallow: false },
      createManyBase: { interceptors: [], decorators: [] },
      updateOneBase: {
        interceptors: [],
        decorators: [],
        allowParamsOverride: false,
        returnShallow: false,
      },
      replaceOneBase: {
        interceptors: [],
        decorators: [],
        allowParamsOverride: false,
        returnShallow: false,
      },
      deleteOneBase: { interceptors: [], decorators: [], returnDeleted: false },
      recoverOneBase: { interceptors: [], decorators: [], returnRecovered: false },
    },
    params: {},
  };

  static load(config: CrudGlobalConfig = {}) {
    const auth = isObjectFull(config.auth) ? config.auth : {};
    const query = isObjectFull(config.query) ? config.query : {};
    const routes = isObjectFull(config.routes) ? config.routes : {};
    const operators = isObjectFull(config.operators) ? config.operators : {};
    const params = isObjectFull(config.params) ? config.params : {};
    const serialize = isObjectFull(config.serialize) ? config.serialize : {};

    if (isObjectFull(config.queryParser)) {
      RequestQueryBuilder.setOptions({ ...config.queryParser });
    }

    if (config.globalInterceptors) {
      routes.createManyBase = appendInterceptors(
        routes.createManyBase,
        config.globalInterceptors,
      );
      routes.createOneBase = appendInterceptors(
        routes.createOneBase,
        config.globalInterceptors,
      );
      routes.getManyBase = appendInterceptors(
        routes.getManyBase,
        config.globalInterceptors,
      );
      routes.getOneBase = appendInterceptors(
        routes.getOneBase,
        config.globalInterceptors,
      );
      routes.updateOneBase = appendInterceptors(
        routes.updateOneBase,
        config.globalInterceptors,
      );
      routes.deleteOneBase = appendInterceptors(
        routes.deleteOneBase,
        config.globalInterceptors,
      );
      routes.replaceOneBase = appendInterceptors(
        routes.replaceOneBase,
        config.globalInterceptors,
      );
      routes.recoverOneBase = appendInterceptors(
        routes.recoverOneBase,
        config.globalInterceptors,
      );
    }

    CrudConfigService.config = deepmerge(
      CrudConfigService.config,
      { auth, query, routes, operators, params, serialize },
      { arrayMerge: (a, b, c) => b },
    );
    CrudConfigService.config.crudRequestBuildInterceptor =
      config.crudRequestBuildInterceptor;
  }
}

function appendInterceptors(route: BaseRouteOptions | null, interceptors: any[]) {
  if (!isObjectFull(route)) {
    route = {};
  }
  route.interceptors = (route.interceptors || []).concat(interceptors);
  return route;
}
