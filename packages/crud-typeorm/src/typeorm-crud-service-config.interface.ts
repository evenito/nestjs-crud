import { CrudRequest, GetManyDefaultResponse } from '@evenito/nestjs-crud';
import { EntityManager } from 'typeorm';

export type TransactionRun<T> = (
  manager: EntityManager,
) => Promise<T | T[] | GetManyDefaultResponse<T>>;
export type TransactionWrapper<T> = (
  manager: EntityManager,
  req: CrudRequest,
  run: TransactionRun<T>,
) => Promise<T>;

export interface TypeormCrudServiceConfig<T> {
  transaction?: TransactionWrapper<T>;
}
