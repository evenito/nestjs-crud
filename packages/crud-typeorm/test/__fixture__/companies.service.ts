import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { TypeOrmCrudService } from '../../../crud-typeorm/src/typeorm-crud.service';
import { Company } from '../../../../integration/crud-typeorm/companies';
import { TypeormCrudServiceConfig } from '@evenito/nestjs-crud-typeorm';

@Injectable()
export class CompaniesService extends TypeOrmCrudService<Company> {
  constructor(@InjectRepository(Company) repo) {
    super(repo);
  }
}

@Injectable()
export class CompaniesService2 extends TypeOrmCrudService<Company> {
  constructor(
    @InjectRepository(Company) repo,
    @Inject('CrudServiceConfig') config: TypeormCrudServiceConfig<Company>,
  ) {
    super(repo, config);
  }
}
