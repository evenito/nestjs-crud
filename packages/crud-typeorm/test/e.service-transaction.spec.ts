import { Controller, INestApplication } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Crud } from '@evenito/nestjs-crud';
import { RequestQueryBuilder } from '@evenito/nestjs-crud-request';
import * as request from 'supertest';
import { Company } from '../../../integration/crud-typeorm/companies';
import { ormConfig } from '../../../integration/crud-typeorm/orm.config';
import { HttpExceptionFilter } from '../../../integration/shared/https-exception.filter';
import { CompaniesService2 } from './__fixture__/companies.service';
import { TypeormCrudServiceConfig } from '@evenito/nestjs-crud-typeorm';
import { Connection } from 'typeorm';

// tslint:disable:max-classes-per-file no-shadowed-variable
describe('#crud-typeorm', () => {
  describe('#service transaction', () => {
    let app: INestApplication;
    let server: any;
    let qb: RequestQueryBuilder;

    let deletionTestId: number;

    @Crud({
      model: { type: Company },
      crudRequestBuildInterceptor: (ctx, crudReq) => {
        const req = ctx.switchToHttp().getRequest();
        if (req) {
          crudReq.extra = {
            role: req.headers['myapp-role'],
            user_id: req.headers['myapp-user'],
          };
        }
        return crudReq;
      },
    })
    @Controller('companies')
    class CompaniesController {
      constructor(public service: CompaniesService2) {}
    }

    beforeAll(async () => {
      const fixture = await Test.createTestingModule({
        imports: [
          TypeOrmModule.forRoot({ ...ormConfig, logging: false }),
          TypeOrmModule.forFeature([Company]),
        ],
        controllers: [CompaniesController],
        providers: [
          { provide: APP_FILTER, useClass: HttpExceptionFilter },
          {
            provide: 'CrudServiceConfig',
            useValue: <TypeormCrudServiceConfig<Company>>{
              transaction: async (manager, req, run) => {
                const role = req.extra?.role || 'rls_tester';
                const user_id = req.extra?.user_id;
                // don't do it like this in production :)
                await manager.query(`SET LOCAL ROLE ${role}`);
                if (user_id) {
                  await manager.query(
                    `SELECT set_config('myapp.user_id', '${user_id}', true);`,
                  );
                }
                return await run(manager);
              },
            },
          },
          CompaniesService2,
        ],
      }).compile();

      app = fixture.createNestApplication();

      await app.init();
      server = app.getHttpServer();

      const connection = app.get<Connection>(Connection);
      await connection.query(
        `DO $$
BEGIN
    CREATE ROLE rls_tester nologin;
    EXCEPTION WHEN DUPLICATE_OBJECT THEN
        RAISE NOTICE 'rls_tester role already exists -- not creating';
END
$$;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO rls_tester;
GRANT SELECT, INSERT, UPDATE, DELETE ON companies TO rls_tester;
ALTER TABLE companies ENABLE ROW LEVEL SECURITY;
DROP POLICY IF EXISTS enforce_user ON companies;
CREATE POLICY enforce_user ON companies TO rls_tester USING (current_setting('myapp.user_id', true) = '1');`,
      );
    });

    beforeEach(() => {
      qb = RequestQueryBuilder.create();
    });

    afterAll(async () => {
      await app.close();
    });

    describe('#getAllBase', () => {
      it('should return an empty list when RLS fails', async () => {
        const res = await request(server).get('/companies');
        expect(res.status).toBe(200);
        expect(res.body.length).toBe(0);
      });
      it('should return an array of all entities', async () => {
        const query = qb.setLimit(4).query();
        const res = await request(server)
          .get('/companies')
          .query(query)
          .set('Myapp-User', '1');
        expect(res.status).toBe(200);
        expect(res.body.length).toBe(4);
      });
    });

    describe('#createOneBase', () => {
      it('should error when RLS fails', async () => {
        const dto = {
          name: 'test10',
          domain: 'test10',
        };
        const res = await request(server)
          .post('/companies')
          .set('Myapp-User', 'not ok')
          .send(dto);
        expect(res.status).toBeGreaterThan(399);
      });
      it('should return saved entity when RLS is used', async () => {
        const dto = {
          name: 'test10',
          domain: 'test10',
        };
        const res = await request(server)
          .post('/companies')
          .set('Myapp-User', '1')
          .send(dto);
        expect(res.status).toBe(201);
        expect(res.body.id).toBeTruthy();
      });
    });

    describe('#createManyBase', () => {
      it('should error when RLS fails', async () => {
        const dto = {
          bulk: [
            {
              name: 'test11',
              domain: 'test11',
            },
            {
              name: 'test12',
              domain: 'test12',
            },
          ],
        };
        const res = await request(server)
          .post('/companies/bulk')
          .set('Myapp-User', 'error')
          .send(dto);
        expect(res.status).toBeGreaterThanOrEqual(400);
      });
      it('should return created entities when RLS succeeds', async () => {
        const dto = {
          bulk: [
            {
              name: 'test11',
              domain: 'test11',
            },
            {
              name: 'test12',
              domain: 'test12',
            },
          ],
        };
        const res = await request(server)
          .post('/companies/bulk')
          .set('Myapp-User', '1')
          .send(dto);
        expect(res.status).toBe(201);
        expect(res.body[0].id).toBeTruthy();
        expect(res.body[1].id).toBeTruthy();
        deletionTestId = res.body[0].id;
      });
    });

    describe('#updateOneBase', () => {
      it('should error when RLS fails', async () => {
        const dto = { name: 'updated0', description: 'foo' };
        const res = await request(server).patch('/companies/1').send(dto);
        expect(res.status).toBeGreaterThanOrEqual(400);
      });

      it('should return updated entity, 1', async () => {
        const dto = { name: 'updated0', description: 'foo' };
        const res = await request(server)
          .patch('/companies/1')
          .set('Myapp-User', '1')
          .send(dto);
        expect(res.status).toBe(200);
        expect(res.body.name).toBe('updated0');
        expect(res.body.description).toBe('foo');
      });
    });

    describe('#replaceOneBase', () => {
      it('should error when RLS fails', async () => {
        const dto = { name: 'updated0', domain: 'domain22' };
        const res = await request(server).put('/companies/333').send(dto);
        expect(res.status).toBeGreaterThanOrEqual(400);
      });
      it('should create entity', async () => {
        const dto = { name: 'updated0', domain: 'domain22' };
        const res = await request(server)
          .put('/companies/333')
          .set('Myapp-User', '1')
          .send(dto);
        expect(res.status).toBe(200);
        expect(res.body.name).toBe('updated0');
      });
      it('should return updated entity, 1', async () => {
        const dto = { name: 'updated0', description: 'new' };
        const res = await request(server)
          .put('/companies/1')
          .set('Myapp-User', '1')
          .send(dto);
        expect(res.status).toBe(200);
        expect(res.body.name).toBe('updated0');
        expect(res.body.description).toBe('new');
      });
    });

    describe('#deleteOneBase', () => {
      it('should return status 404', async () => {
        const res = await request(server).delete('/companies/3333');
        expect(res.status).toBe(404);
      });
      it('should not delete entity if RLS fails', async () => {
        const res = await request(server).delete(`/companies/${deletionTestId}`);
        expect(res.status).toBeGreaterThanOrEqual(400);
      });
      it('should delete entity if RLS succeeds', async () => {
        const res = await request(server)
          .delete(`/companies/${deletionTestId}`)
          .set('Myapp-User', '1');
        expect(res.status).toBe(200);
      });
    });
  });
});
