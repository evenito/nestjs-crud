NestJS CRUD
===========

This is a fork of [@rewiko/crud](https://www.npmjs.com/package/@rewiko/crud).
It is not currently intended for general usage.

[Documentation available in the wiki](https://gitlab.com/evenito/nestjs-crud/-/wikis/home).

Installation
------------

```bash
echo '@evenito:registry=https://gitlab.com/api/v4/packages/npm/' >> .npmrc
npm i @evenito/nestjs-crud
```

Development
-----------

1. Clone the repository
2. Run `yarn bootstrap` to do an one time setup
3. Run `yarn rebuild` to clean build the project

### Run tests

```bash
docker compose up -d                # start a local postgres & redis instance used for testing
yarn rebuild && yarn test:coverage  # rebuild the project after every change and run all tests
yarn format                         # automatically format files that have been changed
```

### Publishing a new version

1. Set `GITLAB_NPM_TOKEN` environment variable [(get the value from GitLab)](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry)
2. Run `yarn lint`
2. Run `yarn pub`
3. Update changelog
